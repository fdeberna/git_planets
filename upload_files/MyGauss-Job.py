###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
# Options specific for a given job
# ie. setting of random number seed and name of output files
#

from Gauss.Configuration import *
from Gaudi.Configuration import *
from Configurables import Gauss

## # Here are beam settings as for various nu (i.e. mu and Lumi per bunch with
## # 25 ns bunch spacing are given
## This is the Run3 default luminosity 
##   nu=7.6 (i.e. mu=5.31, Lumi=2.0*(10**33) with2400 colliding bunches)
importOptions("$APPCONFIGOPTS/Gauss/Beam7000GeV-md100-nu7.6-HorExtAngle.py")
#
## Nominal Lumi to begin with - and for faster tests
##   nu=3.8 (i.e. mu=2.66, Lumi=1.0*(10**33) )
#importOptions("$APPCONFIGOPTS/Gauss/Beam7000GeV-md100-nu3.8-HorExtAngle.py")
#
## For robustness studies
##   nu=11.4 (i.e. mu=4, Lumi=1.77*(10**33) )
#importOptions("$APPCONFIGOPTS/Gauss/Beam7000GeV-md100-nu11.4-HorExtAngle.py")


## # The spill-over is off for quick tests
## to enable spill-over use the followign options
#importOptions("$APPCONFIGOPTS/Gauss/EnableSpillover-25ns.py")

## # The data type for now is "Upgrade" and set in the baseline options.
## the default configuration of the detector is set and can be changed in the
## following file
importOptions("$GAUSSOPTS/Gauss-Upgrade-Baseline.py")

importOptions('$DECFILESROOT/options/11114001.py')
## # The latest supported global tag for 2022
## for this version of Gauss including selecting the upgrade branch in the database.


LHCbApp().DDDBtag   = "MyBranch"
LHCbApp().CondDBtag = "master"

#--Generator phase, set random numbers
GaussGen = GenInit("GaussGen")
GaussGen.FirstEventNumber = 1
GaussGen.RunNumber        = 1082

#--Number of events
nEvts = 5
LHCbApp().EvtMax = nEvts




#Gauss().OutputType = 'NONE'
#Gauss().Histograms = 'NONE'
#--Set name of output files for given job (uncomment the lines)
#  Note that if you do not set it Gauss will make a name based on event type,
#  number of events and the date
#idFile = 'GaussTest'
#HistogramPersistencySvc().OutputFile = idFile+'-histos.root'
#
#OutputStream("GaussTape").Output = "DATAFILE='PFN:%s.sim' TYP='POOL_ROOTTREE' OPT='RECREATE'"%idFile

#GenMonitor = GaudiSequencer( "GenMonitor" )
#SimMonitor = GaudiSequencer( "SimMonitor" )
#GenMonitor.Members += [ "GaussMonitor::CheckLifeTimeHepMC/HepMCLifeTime" ]
#SimMonitor.Members += [ "GaussMonitor::CheckLifeTimeMC/MCLifeTime" ]

